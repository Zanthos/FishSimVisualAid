import javax.swing.JPanel;
import javax.swing.JComboBox;
import java.awt.*;
import java.io.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

public class Canvas extends JPanel
{
    private static boolean paused = true;
    private static int speed = 50;
    private int numFish = 0;
    private BufferedImage img = null;
    private BufferedImage sharkImg = null;
    public Canvas()
    {
        try{
            img = ImageIO.read(new File("fishy.png"));
            sharkImg = ImageIO.read(new File("shark.png"));
        } catch (IOException e) {}

        KeyListener listener = new KeyListener() {
            @Override
            public void keyPressed(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.VK_SPACE)
                {
                    pauseToggle();
                }
                else if (event.getKeyCode() == KeyEvent.VK_UP && speed > 0)
                {
                    speed-=5;
                }
                else if (event.getKeyCode() == KeyEvent.VK_DOWN && speed < 100)
                {
                    speed+=5;
                }
                else if (event.getKeyCode() == KeyEvent.VK_RIGHT)
                {
                    Utilities.nextGen();
                }
            }
            @Override
            public void keyReleased(KeyEvent event) {
            }
            @Override
            public void keyTyped(KeyEvent event) {
            }
        };
        addKeyListener(listener);
        setFocusable(true);
    }

    public static Graphics2D page;
    public void paint (Graphics g)
    {
        page = (Graphics2D) g;
        super.paintComponent(g);
        long newTime = System.nanoTime();
        if ((newTime - Main.oldTime) / 1000000 > speed && !paused)
        {
            Utilities.read();
            Main.oldTime = newTime;
        }
        numFish = 0;
        g.setColor(Color.blue);
        int halfWidth = img.getWidth() / 2;
        int halfHeight = img.getHeight() / 2;
        for (Fish fishy : Utilities.getFish())
        {
            int x = (int)((fishy.getX()+50.0) * 10.0);
            int y = (int)((fishy.getY()+50.0) * 10.0);
            AffineTransform trans = new AffineTransform();
            trans.translate(x-halfWidth, y-halfHeight);
            trans.rotate(Math.PI + fishy.getAngle(), halfWidth, halfHeight);
            // page.drawImage(img, x-(fishSize/2), y-(fishSize/2), null);
            page.drawRenderedImage(img, trans);
            numFish ++;
            //page.fillOval((int)(fishy.getX()+ 50.0) * 10, (int)(fishy.getY() + 50.0) * 10, 10, 10);
        }
        halfWidth = sharkImg.getWidth() / 2;
        halfHeight = sharkImg.getHeight() / 2;
        g.setColor(Color.gray);
        for (Shark sharky : Utilities.getSharkies())
        {
            int x = (int)((sharky.getX()+50.0) * 10.0);
            int y = (int)((sharky.getY()+50.0) * 10.0);
            AffineTransform trans = new AffineTransform();
            trans.translate(x-halfWidth, y-halfHeight);
            trans.rotate(Math.PI + sharky.getAngle(), halfWidth, halfHeight);
            page.drawRenderedImage(sharkImg, trans);
            // page.fillOval((int)(sharky.getX()+ 50.0) * 10,(int)(sharky.getY() + 50.0) * 10, 25, 25);
        }

        g.setColor(Color.black);
        page.drawString( "Gen: " + Utilities.getGeneration() + " | Fish left: " + numFish + " | Speed: " + ((speed*-1)+100), 10, 20);

    }

    public void actionPerformed(ActionEvent e)
    {
        JComboBox cb = (JComboBox)e.getSource();
        Utilities.readerInit((String)cb.getSelectedItem());
    }

    public static void draw (int x, int y, int diameter)
    {
        page.fillOval(x, y, diameter, diameter);
    }

    public static void pauseToggle()
    {
        paused = !paused;
    }
}
