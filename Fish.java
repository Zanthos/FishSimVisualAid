public class Fish
{
    private double X;
    private double Y;
    private double angle;
    public Fish(double x, double y, double angle)
    {
        X = x;
        Y = y;
        this.angle = angle;
    }

    public double getX()
    {
        return X;
    }

    public double getY()
    {
        return Y;
    }

    public double getAngle()
    {
        return angle;
    }
}
