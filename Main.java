public class Main
{
    public static long oldTime = System.nanoTime();
    public static boolean shouldRun = true;
    public static void main(String[] args)
    {
        System.out.println("initializing...");
        if (args.length > 0)
            Utilities.init(args[0]);
        else Utilities.init("");
        while(shouldRun)
        {
            Utilities.update();
        }
        Utilities.exit();
    }
}
