# Fish Simulation Visualization

The purpose of this project is to take JSON generated from Stuart's NEAT simulation of fish and visualize the data in a way that we can easily see the changes being made between generations. To launch the simulation, run `run.bat {file_name}` with {file_name} being the file that you wish to visualize as the argument.

## Controls
* SPACE - Toggle pause
* RIGHT - Advance to next generation
* UP - Speed up the fps
* DOWN - Slow down the fps