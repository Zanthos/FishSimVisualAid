import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Container;
import java.awt.BorderLayout;
import java.io.*;
import java.util.*;
import org.json.*;


public class Utilities
{
    public static Canvas c = new Canvas();
    private static List<Fish> fish = new ArrayList<Fish>();
    private static List<Shark> sharkies = new ArrayList<Shark>();
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 1000;
    public static BufferedReader br;
    private static String currentFile;
    private static int generation;

    public static void init(String file)
    {
        if (file != "")
            currentFile = file;
        else currentFile = "fishsimVisuals.data";
        readerInit(currentFile);
        JFrame frame = new JFrame("Matrix");
            frame.add(c);
            frame.setSize(WIDTH, HEIGHT);
            frame.setVisible(true);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        read();
        System.out.println("finished init");
    }

    public static void readerInit(String file)
    {
        try {

        br = new BufferedReader(new FileReader(file));
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }
    }

    public static void read()
    {
        fish.clear();
        sharkies.clear();
        try{
            String line = br.readLine();
            if (line == null)
            {
                System.out.println("nothing to read");
                //paused = true;
                readerInit(currentFile);
                update();
            }
            JSONObject scene = new JSONObject(line);
            JSONArray networks = scene.getJSONArray("fish");
            for(int i=0;i<networks.length();i++)
            {
                JSONObject fishy = networks.getJSONObject(i);
                fish.add(new Fish(fishy.getDouble("x"), fishy.getDouble("y"), fishy.getDouble("angle")));
            }
            JSONArray predators = scene.getJSONArray("sharks");
            for(int i=0;i<predators.length();i++)
            {
                JSONObject sharky = predators.getJSONObject(i);
                sharkies.add(new Shark(sharky.getDouble("x"), sharky.getDouble("y"), sharky.getDouble("angle")));
            }
            generation = scene.getInt("generation");

        } catch (IOException e) {
            e.printStackTrace();
            Main.shouldRun = false;
            if (br != null)
                try{
                    br.close();
                }
                catch (IOException i)
                {

                }
            return;
        }
    }

    public static void nextGen()
    {
        int gen = generation;
        while (gen == generation)
        {
            read();
        }
    }

    public static void lastGen()
    {
        int gen = generation;
    }

    public static void update()
    {
        c.repaint();
    }

    public static void exit()
    {
        System.exit(0);
    }

    public static List<Fish> getFish()
    {
        return fish;
    }

    public static List<Shark> getSharkies()
    {
        return sharkies;
    }

    public static int getGeneration()
    {
        return generation;
    }
}
